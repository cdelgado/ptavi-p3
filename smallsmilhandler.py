#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    def __init__(self):
        """ Constructor. Inicializamos las variables """
        self.lista = []
        self.width = " "
        self.height = " "
        self.backgroundcolor = " "
        self.id = " "
        self.top = " "
        self.bottom = " "
        self.left = " "
        self.right = " "
        self.src = " "
        self.region = " "
        self.begin = " "
        self.dur = " "

    def startElement(self, name, attrs):
        """ Método que se llama cuando se abre una etiqueta """
        if name == 'root-layout':
            self.width = attrs.get('width', " ")
            self.height = attrs.get('height', " ")
            self.backgroundcolor = attrs.get('backgroundcolor', " ")
            self.lista.append({'etiqueta': name,
                               'width': self.width,
                               'height': self.height,
                               'backgroundcolor': self.backgroundcolor})
        elif name == 'region':
            self.id = attrs.get('id', " ")
            self.top = attrs.get('top', " ")
            self.bottom = attrs.get('bottom', " ")
            self.left = attrs.get('left', " ")
            self.right = attrs.get('right', " ")
            self.lista.append({'etiqueta': name,
                               'id': self.id,
                               'top': self.top,
                               'bottom': self.bottom,
                               'left': self.left,
                               'right': self.right})
        elif name == 'img':
            self.src = attrs.get('src', " ")
            self.region = attrs.get('region', " ")
            self.begin = attrs.get('begin', " ")
            self.dur = attrs.get('dur', " ")
            self.lista.append({'etiqueta': name,
                               'src': self.src,
                               'region': self.region,
                               'begin': self.begin,
                               'dur': self.dur})
        elif name == 'audio':
            self.src = attrs.get('src', " ")
            self.begin = attrs.get('begin', " ")
            self.dur = attrs.get('dur', " ")
            self.lista.append({'etiqueta': name,
                               'src': self.src,
                               'begin': self.begin,
                               'dur': self.dur})
        elif name == 'textstream':
            self.src = attrs.get('src', " ")
            self.region = attrs.get('region', " ")
            self.lista.append({'etiqueta': name,
                               'src': self.src,
                               'region': self.region})

    def get_tags(self):
        return self.lista


if __name__ == "__main__":
    """Programa principal"""
    parser = make_parser()
    Handler = SmallSMILHandler()
    parser.setContentHandler(Handler)
    parser.parse(open('karaoke.smil'))
    list = Handler.get_tags()
    print(list)
