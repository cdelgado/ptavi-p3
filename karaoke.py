#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import smallsmilhandler
import json
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from urllib.request import urlretrieve


class KaraokeLocal():
    def __init__(self):
        parser = make_parser()
        Handler = smallsmilhandler.SmallSMILHandler()
        parser.setContentHandler(Handler)
        self.list = Handler.get_tags()
        try:
            parser.parse(open(file))
        except IndexError:
            sys.exit("Usage:python3 karaoke.py karaoke.smil.")

    def __str__(self):
        for elements in self.list:
            texto = ''
            for etiqueta in elements:
                if etiqueta == "etiqueta":
                    texto += elements[etiqueta] + '\t'
                elif elements[etiqueta] != "":
                    texto += etiqueta + '="' + elements[etiqueta] + '" \t'
            texto = texto[:-1] + '\n'
            print(texto)

    def do_json(self, file):
        fichJson = file.replace('.smil', '.json')
        with open(fichJson, "w") as outfich:
            json.dump(self.list, outfich)

    def do_local(self):
        for elements in self.list:
            for etiqueta in elements:
                if etiqueta == 'src':
                    url = elements[etiqueta]
                    if url.startswith('http://'):
                        file = url.split('/')[-1]
                        urlretrieve(url, file)
                        elements[etiqueta] = file


if __name__ == "__main__":
    try:
        file = (sys.argv[1])
    except IndexError:
        sys.exit("Usage:python3 karaoke.py karaoke.smil.")

    karaoke = KaraokeLocal()
    karaoke.do_json(file)
    karaoke.do_local()
    print(karaoke)
